@extends('layouts.app')

@section('js')
<script>

</script>
@endsection

@section('nav-bar-items')
    <li class="nav-item">
        <a class="nav-link link-navbar" href="#referir"><b>Referir a un paciente</b></a>
    </li>
    <li class="nav-item">
        <a class="nav-link link-navbar" href="#tus-referidos"><b>Tus referidos</b></a>
    </li>
    <li class="nav-item">
        <a class="nav-link link-navbar" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    </li>
@endsection

@section('content')

    <div data-spy="scroll" data-target=".navbar" >
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-sm-4">
                    <img src="{{url('/images/doctor.png')}}" alt="Doctor"/>
                </div>
                <div class="col-sm-6 h-100" style="margin-top: auto; margin-bottom: auto;">
                    <div class="justify-content-center">
                        ¡Hola Dr. <b>{{ $user->name }}</b>!
                        <br>
                        <br>
                        En esta página puedes referir a tus pacientes con Tani, para que encuentren procedimientos médicos confiables y a precio justo.
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid container-custom" id="referir" style="height: 650px">
            <h2>Registra un nuevo paciente referido</h2>
            <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
            <script>
                hbspt.forms.create({
                    region: "na1",
                    portalId: "9336468",
                    formId: "6cbfe340-c9b7-4ebd-b5e4-037d91e4a17e"
                });
            </script>
        </div>

        <div class="container-fluid container-custom-white" id="tus-referidos" style="height: 600px">
            <h2>Tus referidos</h2>
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card box-custom">
                                    <div class="card-body">
                                        <div class="box-title box-color-yellow">Paciente referido</div>
                                        @foreach($referidos['referidos'] as $r)
                                            <div class="card-doctor card-doctor-yellow">
                                                {{ \Carbon\Carbon::parse($r['properties']['createdate'])->format('d/m/Y') }}<br>
                                                {{ $r['properties']['firstname'].' '.$r['properties']['lastname'] }}<br>
                                                {{ $r['tratamiento'] }}<br>
                                                Creado {{ \Carbon\Carbon::parse($r['properties']['createdate'])->format('d/m/Y') }}<br>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card box-custom">
                                    <div class="card-body">
                                        <div class="box-title box-color-orange">Agendó cita con especialista</div>
                                        @foreach($referidos['agendaronCitaEspecialista'] as $r)
                                            <div class="card-doctor card-doctor-orange">
                                                {{ \Carbon\Carbon::parse($r['properties']['createdate'])->format('d/m/Y') }}<br>
                                                {{ $r['properties']['dealname'] }}<br>
                                                {{ $r['tratamiento'] }}<br>
                                                Creado {{ \Carbon\Carbon::parse($r['properties']['createdate'])->format('d/m/Y') }}<br>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card box-custom">
                                    <div class="card-body">
                                        <div class="box-title box-color-plum">Tuvo cita exitosa con especialista</div>
                                        @foreach($referidos['citaExitosaEspecialista'] as $r)
                                            <div class="card-doctor card-doctor-plum">
                                                {{ \Carbon\Carbon::parse($r['properties']['createdate'])->format('d/m/Y') }}<br>
                                                {{ $r['properties']['dealname'] }}<br>
                                                {{ $r['tratamiento'] }}<br>
                                                Creado {{ \Carbon\Carbon::parse($r['properties']['createdate'])->format('d/m/Y') }}<br>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card box-custom">
                                    <div class="card-body">
                                        <div class="box-title box-color-blue">Paciente interesado en el procedimiento</div>
                                        @foreach($referidos['pacientesInteresadosProcedimientos'] as $r)
                                            <div class="card-doctor card-doctor-blue">
                                                {{ \Carbon\Carbon::parse($r['properties']['createdate'])->format('d/m/Y') }}<br>
                                                {{ $r['properties']['dealname'] }}<br>
                                                {{ $r['tratamiento'] }}<br>
                                                Creado {{ \Carbon\Carbon::parse($r['properties']['createdate'])->format('d/m/Y') }}<br>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card box-custom">
                                    <div class="card-body">
                                        <div class="box-title box-color-skyblue">Paciente agendó procedimiento</div>
                                        @foreach($referidos['pacientesAgendaronProcedimientos'] as $r)
                                            <div class="card-doctor card-doctor-skyblue">
                                                {{ \Carbon\Carbon::parse($r['properties']['createdate'])->format('d/m/Y') }}<br>
                                                {{ $r['properties']['dealname'] }}<br>
                                                {{ $r['tratamiento'] }}<br>
                                                Creado {{ \Carbon\Carbon::parse($r['properties']['createdate'])->format('d/m/Y') }}<br>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card box-custom">
                                    <div class="card-body">
                                        <div class="box-title box-color-jungle">Paciente realizó procedimiento</div>
                                        @foreach($referidos['pacientesRealizaronProcedimientos'] as $r)
                                            <div class="card-doctor card-doctor-jungle">
                                                {{ \Carbon\Carbon::parse($r['properties']['createdate'])->format('d/m/Y') }}<br>
                                                {{ $r['properties']['dealname'] }}<br>
                                                {{ $r['tratamiento'] }}<br>
                                                Creado {{ \Carbon\Carbon::parse($r['properties']['createdate'])->format('d/m/Y') }}<br>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

@endsection

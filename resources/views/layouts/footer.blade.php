<!-- Footer -->
<footer class="page-footer font-small blue pt-4">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">

        <!-- Grid row -->
        <div class="row">

            <!-- Grid column -->
            <div class="col-md-4 mt-md-0 mt-3">

                <!-- Content -->
                <div class="img-logo-footer">
                    <img src="{{url('/images/tani_logo_white.png')}}" alt="Tani" width="70,21px" height="32px"/>
                </div>
                <p>Tani es la plataforma que te permite encontrar el procedimiento médico que necesitas de forma fácil, rápida, segura y a un precio justo.</p>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none pb-3">

            <!-- Grid column -->
            <div class="col-md-4 mb-md-0 mb-3">

                <!-- Links -->
                <ul class="list-unstyled">
                    <li>
                        <a href="https://tanisalud.com/conocenos" class="link-footer">Conócenos</a>
                    </li>
                    <li>
                        <a href="https://tanisalud.com/nuestros-procedimientos" class="link-footer">Nuestros procedimientos</a>
                    </li>
                    <li>
                        <a href="https://tanisalud.com/nuestros-medicos" class="link-footer">Nuestros Médicos</a>
                    </li>
                    <li>
                        <a href="https://tanisalud.com/faq" class="link-footer">Preguntas frecuentes</a>
                    </li>
                    <li>
                        <a href="https://tanisalud.com/contacto" class="link-footer">Contacto</a>
                    </li>
                </ul>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4 mb-md-0 mb-3">

                <!-- Links -->
                <h5>Si tienes preguntas o dudas, llámanos o mándanos un WhatsApp:</h5>

                <div class="tel-number">
                    <img src="{{url('/images/phone_icon.png')}}" alt="phone" width="16px" height="16px"/>
                    55 3708 4009
                </div>

                <div>
                    <a href="https://www.instagram.com/tani_salud/" class="social-network">
                        <img src="{{url('/images/instagram_logo.png')}}" alt="instagram" width="40px" height="40px"/>
                    </a>
                    <a href="https://www.facebook.com/tanisalud/" class="social-network">
                        <img src="{{url('/images/facebook_logo.png')}}" alt="facebook" width="40px" height="40px"/>
                    </a>
                </div>


            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <div class="container-fluid text-center text-md-left" style="padding-bottom: 43px">
        <div class="row">
            <!-- Grid column -->
            <div class="col-md-8 mt-md-0 mt-3">
                <div class="footer-copyright text-left">
                    @2021 Tani Salud
                </div>
            </div>
            <div class="col-md-2 mt-md-0 mt-3 text-right">
                <a class="tyc" href="https://tanisalud.com/terminos-y-condiciones">Términos y condiciones</a>
            </div>
            <div class="col-md-2 mt-md-0 mt-3">
                <a class="tyc" href="https://tanisalud.com/politica-de-privacidad">Política de privacidad</a>
            </div>
            <!-- Copyright -->
        </div>
    </div>

</footer>
<!-- Footer -->
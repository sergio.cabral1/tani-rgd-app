<style>
    body {
        overflow-x: hidden !important;
        font-family: 'Open Sans', sans-serif !important;
        color: #0B2F40;
        font-style: normal;
        font-weight: normal;
        font-size: 16px;
        line-height: 130%;
    }
    h2 {
        font-family: 'Baloo Tammudu 2', sans-serif !important;;
        font-style: normal;
        font-weight: 600;
        font-size: 24px;
        line-height: 130%;
        padding-top: 29px;
    }
    label {
        font-family: 'Baloo Tammudu 2', sans-serif !important;;
        font-style: normal;
    }
    .btn-tani {
        color: black;
        background-color: #F8F7F1;
        border-color: #F8F7F1;
    }
    .container-custom{
        background-color: #F8F7F1;
        box-shadow: 0px 6px 8px rgba(11, 47, 64, 0.1);
    }
    .container-custom-white {
        box-shadow: 0px 6px 8px rgba(11, 47, 64, 0.1);
    }
    .container-custom-blue {
        background-color: #0B2F40;;
    }
    .img-logo{
        padding-left: 150px;
    }
    .box-custom{
        background-color: #F8F7F1;
        border-radius: 5px 5px 5px 30px;
        height: 330px;
    }
    .box-title {
        font-family: 'Baloo Tammudu 2', sans-serif !important;;
        font-style: normal;
        font-weight: 600;
        font-size: 16px;
        line-height: 130%;
        text-align: center;
    }
    .box-color-yellow {
        color: #FFB93F;
    }
    .box-color-orange {
        color: #F46338;
    }
    .box-color-plum {
        color: #8F3985;
    }
    .box-color-blue {
        color: #2E9ED0;
    }
    .box-color-skyblue {
        color: #5AC4D0;
    }
    .box-color-blackblue {
        color: #0B2F40;
    }
    .box-color-jungle {
        color: #37A989;
    }
    .card-doctor {
        text-align: right;
        color: white;
        border-radius: 5px 5px 5px 30px;
    }
    .card-doctor-yellow {
        background: #FFB93F;
    }
    .card-doctor-orange {
        background: #F46338;
    }
    .card-doctor-plum {
        background: #8F3985;
    }
    .card-doctor-blue {
        background: #2E9ED0;
    }
    .card-doctor-skyblue {
        background: #5AC4D0;
    }
    .card-doctor-blackblue {
        background: #0B2F40;
    }
    .card-doctor-jungle {
        background: #37A989;
    }
    .gain {
        height: 60px;
        width: 90px;
        left: 7px;
        border-radius: 30px;
        right: 7.62%;
        bottom: 41.18%;
        background: #37A989;
        font-family: 'Baloo Tammudu 2', sans-serif !important;
        font-style: normal;
        font-weight: 600;
        font-size: 15px;
        line-height: 130%;
        text-align: center;
        padding-top: 24px;
        color: #FFFFFF;
    }
    .gain-text {
        font-family: 'Open Sans', sans-serif !important;
        font-style: normal;
        font-weight: bold;
        font-size: 12px;
        line-height: 115%;
        text-align: center;
    }
    .gain-box {
        text-align: center;

    }
    footer {
        background-color: #0B2F40;
        color: white;
    }
    footer .link-footer {
        font-family: 'Open Sans', sans-serif !important;
        font-style: normal;
        font-weight: bold;
        font-size: 16px;
        line-height: 130%;
        color: #FFFFFF;
    }
    footer li {
        padding-bottom: 8px;
        padding-top: 8px;
    }
    footer .tyc {
        font-family: 'Open Sans', sans-serif !important;
        font-style: italic;
        font-weight: normal;
        font-size: 12px;
        line-height: 130%;
        color: #FFFFFF;
    }
    footer .tel-number {
        font-family: 'Open Sans', sans-serif !important;
        font-style: normal;
        font-weight: bold;
        font-size: 16px;
        line-height: 130%;
        color: #FFFFFF;
        padding-bottom: 24px;
        padding-top: 24px;
    }
    footer .footer-copyright {
        font-family: 'Open Sans', sans-serif !important;
        font-style: italic;
        font-weight: normal;
        font-size: 12px;
        line-height: 130%;
    }
    footer .social-network {
        padding: 12px;
    }
    .img-logo-footer {
        padding-top: 20px;
        padding-bottom: 20px;
    }
    .nav-bar-custom {
        background-color: #F8F7F1;
        box-shadow: 0px 6px 8px rgba(11, 47, 64, 0.1);
        padding-top: 25px;
        padding-bottom: 25px;
    }
    .nav-link {
        color: #0B2F40;
    }
    .link-navbar {
        font-family: Open Sans;
        font-style: normal;
        font-weight: bold;
        font-size: 14px;
        line-height: 130%;
        color: #0B2F40;
    }
</style>

<!-- Navbar -->
<nav class="navbar navbar-expand-lg nav-bar-custom">
    <a class="navbar-brand" href="{{ route('login') }}"><img class="img-logo" src="{{url('/images/tani_logo.png')}}" alt="Tani"
                                          height="35px"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            @yield('nav-bar-items')


        </ul>
    </div>
</nav>
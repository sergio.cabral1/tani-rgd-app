<?php

namespace App\Http\Controllers;

use App\Services\HubspotService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    protected $hubspotService;

    public function __construct(HubspotService $hubspotService)
    {
        $this->hubspotService = $hubspotService;
    }

    public function __invoke()
    {
        $user = Auth::user();
        $referidos = $this->hubspotService->getReferidos($user);

        return view('home.home', [
            'user' => $user,
            'referidos' => $referidos
        ]);
    }
}

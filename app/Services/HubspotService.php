<?php


namespace App\Services;


use App\Models\User;
use Illuminate\Support\Facades\Http;

/**
 * Class HubspotService
 * @package App\Services
 */
class HubspotService
{
    /**
     * @var mixed
     */
    private $apiKey;

    public function __construct()
    {
        $this->apiKey = env('API_KEY_HUBSPOT');
    }

    private function getPacientesReferidos(User $user)
    {
        $bodyArray = json_decode('{
              "filterGroups":[
                   {
                     "filters":[
                       {
                         "propertyName": "referrer_email",
                         "operator": "EQ",
                         "value": "' . $user->email . '"
                       }
                     ]
                   }
             ],
             "properties": [
                  "amelia_service", "createdate", "firstname", "lastname"
             ]
        }', true);

        $response = Http::withHeaders([
            'Content-Type' => 'application/json'
        ])->post('https://api.hubapi.com/crm/v3/objects/contacts/search?hapikey=' . $this->apiKey,
            $bodyArray);

        return $response->json();
    }

    private function getDealsAsociados(string $contactId)
    {
        $bodyArray = json_decode('{
            "filters": [
              {
                "propertyName": "associations.contact",
                "operator": "EQ",
                "value": "' . $contactId . '"
              }
            ]
        }', true);

        $response = Http::withHeaders([
            'Content-Type' => 'application/json'
        ])->post('https://api.hubapi.com/crm/v3/objects/deals/search?hapikey=' . $this->apiKey,
            $bodyArray);

        return $response->json();
    }

    /**
     * @param User $user
     * @return array
     */
    public function getReferidos(User $user)
    {
        $pacientesReferidos = $this->getPacientesReferidos($user);

        $referidos = $pacientesReferidos['results'];
        $agendaronCitaEspecialista = [];          // appointmentscheduled
        $citaExitosaEspecialista = [];            // contractsent
        $pacientesInteresadosProcedimientos = []; // 11348627
        $pacientesAgendaronProcedimientos = [];   // 11348628
        $pacientesRealizaronProcedimientos = [];  // 11350874


        foreach ($referidos as $key => $referido) {
            $deals = $this->getDealsAsociados($referido['id']);
            $tratamiento = TratamientosDictionary::TRATAMIENTOS[$referido['properties']['amelia_service']] ?? '-';
            $referidos[$key]['tratamiento'] = $tratamiento;

            foreach ($deals['results'] as $deal) {
                $deal['tratamiento'] = $tratamiento;
                $dealstage = $deal['properties']['dealstage'];

                if ($dealstage == 'appointmentscheduled') {
                    $agendaronCitaEspecialista[] = $deal;
                } elseif ($dealstage == 'contractsent') {
                    $citaExitosaEspecialista[] = $deal;
                } elseif ($dealstage == '11348627') {
                    $pacientesInteresadosProcedimientos[] = $deal;
                } elseif ($dealstage == '11348628') {
                    $pacientesAgendaronProcedimientos[] = $deal;
                } elseif ($dealstage == '11350874') {
                    $pacientesRealizaronProcedimientos[] = $deal;
                }
            }
        }

        return [
            'referidos' => $referidos,
            'agendaronCitaEspecialista' => $agendaronCitaEspecialista,
            'citaExitosaEspecialista' => $citaExitosaEspecialista,
            'pacientesInteresadosProcedimientos' => $pacientesInteresadosProcedimientos,
            'pacientesAgendaronProcedimientos' => $pacientesAgendaronProcedimientos,
            'pacientesRealizaronProcedimientos' => $pacientesRealizaronProcedimientos
        ];
    }

}
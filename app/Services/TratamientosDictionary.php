<?php


namespace App\Services;


class TratamientosDictionary
{
    const TRATAMIENTOS = [
        '2' => 'Colelap',
        '5' => 'Remoción de lunares & verrugas',
        '6' => 'Endoscopía',
        '7' => 'Colonoscopía',
        '14' => 'Hernioplastia',
        '19' => 'Cirugía bariátrica',
        '20' => 'Rinoplastía',
        '22' => 'Bichectomía',
        '23' => 'Ginecomastia',
        '24' => 'Abdominoplastía',
        '25' => 'Liposucción',
        '26' => 'Lipoescultura',
        '27' => 'Mamoplastía (Aumento de senos)',
        '28' => 'Mamoplastía (Reducción de senos)',
        '29' => 'Otoplastía',
        '30' => 'Mentoplastia',
        '31' => 'Extracción de implante',
        '32' => 'Lipopapada',
        '33' => 'Braquioplastia',
        '35' => 'Hernioplastia Abdominal',
        '36' => 'Venas varices',
        '37' => 'Varicocelectomía',
        '38' => 'Circuncisión',
        '39' => 'Infiltraciones',
        '40' => 'Corrección de juanete',
        '41' => 'Hernioplastia discal',
        '42' => 'LASIK',
        '43' => 'Hemorroides',
        '44' => 'Cirugía del túnel carpiano',
        '45' => 'Vasectomía',
        '46' => 'Implante capilar',
        '47' => 'Cataratas',
        '48' => 'Operación de calculos renales',
        'Otro' => 'Otro'
    ];
}